package com.example.examen_c1

class CuentaBanco {

    var numeroCuenta: Int = 0;
    var nombre : String = "";
    var banco : String = "";
    var saldo : Float = 0.0f;

    constructor(){
        this.numeroCuenta = 0;
        this.nombre = "";
        this.banco = "";
        this.saldo = 0.0f;
    }
    constructor(
        numeroCuenta : Int,
        nombre : String,
        banco : String,
        saldo : Float
    ){
        this.numeroCuenta = numeroCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    fun depositar(cantidadSaldo: Float){
        this.saldo += cantidadSaldo;
    }

    fun retirar(cantidadRetirar: Float) : Boolean{
        if(this.saldo >= cantidadRetirar){

            this.saldo -= cantidadRetirar;
            
            return true;
        }
        else return false;
    }

}