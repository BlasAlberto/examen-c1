package com.example.examen_c1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class cuentaBancoActivity : AppCompatActivity() {

    // Declaracion de componentes
    private lateinit var lblBanco : TextView;
    private lateinit var lblNombre : TextView;
    private lateinit var lblSaldo : TextView;
    private lateinit var txtCantidad : EditText;
    private lateinit var btnDepositar : Button;
    private lateinit var btnRetirar : Button;
    private lateinit var btnRegresar : Button;

    // Declaración de clases
    private lateinit var cuentaBanco: CuentaBanco;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuenta_banco)

        // Instanciamientos
        this.instanciamientoComponentes()
        this.cuentaBanco = CuentaBanco()

        // Obtener datos con el paquete de información enviado
        var paqueteInf:Bundle = intent.extras as Bundle
        var strNumeroCuenta:String = paqueteInf!!.getString("strNumeroCuenta").toString()
        var strNombre:String = paqueteInf!!.getString("strNombre").toString()
        var strBanco:String = paqueteInf!!.getString("strBanco").toString()
        var strSaldo:String = paqueteInf!!.getString("strSaldo").toString()

        // Obtener nombre desde los Strings guardados
        var nombreBanco: String
        nombreBanco = applicationContext.resources.getString(R.string.strBanco)

        // Mostrar información
        this.lblBanco.setText(nombreBanco)
        this.lblNombre.setText(strNombre)
        this.lblSaldo.setText(strSaldo)

        // Construir objeto: CuentaBanco
        this.cuentaBanco.numeroCuenta = strNumeroCuenta.toInt()
        this.cuentaBanco.banco = strBanco
        this.cuentaBanco.nombre = strNombre
        this.cuentaBanco.saldo = strSaldo.toFloat()

        // Eventos clics
        this.btnDepositar.setOnClickListener { this.clic_btnDepositar() }
        this.btnRetirar.setOnClickListener { this.clic_btnRetirar() }
        this.btnRegresar.setOnClickListener { this.clic_btnRegresar() }
    }


    // Enlazar componentes con los layouts
    private fun instanciamientoComponentes(){
        this.lblBanco = findViewById(R.id.lblNombreBanco)
        this.lblNombre = findViewById(R.id.lblNombreCliente)
        this.lblSaldo = findViewById(R.id.lblSaldo)

        this.txtCantidad = findViewById(R.id.txtCantidad)

        this.btnDepositar = findViewById(R.id.btnDeposito)
        this.btnRetirar = findViewById(R.id.btnRetiro)
        this.btnRegresar = findViewById(R.id.btnRegresar)
    }


    // Evento clic del Button: btnDepositar
    private fun clic_btnDepositar(){
        var strCantidad: String = this.txtCantidad.text.toString()

        if(!strCantidad.equals("")){

            this.cuentaBanco.depositar(strCantidad.toFloat())
            this.lblSaldo.setText(this.cuentaBanco.saldo.toString())
            this.txtCantidad.setText("")

            Toast.makeText(applicationContext, "Deposito COMPLETADO", Toast.LENGTH_SHORT).show()

        }
        else Toast.makeText(applicationContext, "Favor de ingresar la cantidad", Toast.LENGTH_SHORT).show()
    }


    // Evento clic del Button: btnRetirar
    private fun clic_btnRetirar(){
        var strCantidad: String = this.txtCantidad.text.toString()

        if(!strCantidad.equals("")){

            if(this.cuentaBanco.retirar(strCantidad.toFloat())){
                Toast.makeText(applicationContext, "Retiro COMPLETADO", Toast.LENGTH_SHORT).show()
                this.lblSaldo.setText(this.cuentaBanco.saldo.toString())
                this.txtCantidad.setText("")
            }
            else{
                Toast.makeText(applicationContext, "Saldo insuficiente", Toast.LENGTH_SHORT).show()
            }

        }
        else Toast.makeText(applicationContext, "Favor de ingresar la cantidad", Toast.LENGTH_SHORT).show()
    }


    // Evento clic del Button: btnRetirar
    private fun clic_btnRegresar(){
        var confirmar = AlertDialog.Builder(this)

        confirmar.setTitle("NACIONAL SOMEX")
        confirmar.setMessage("¿Decea regresar al inicio?")

        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }

        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }

        confirmar.show()
    }
}